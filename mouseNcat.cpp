///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Waylon Bader <wbader@hawaii.edu>
/// @date    27 Jan 2022 
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {

   if(argc > 1)
   {
	   //std::cout << "argc = " << (argc - 1) << " Reversing the input:\n"; 
      for(int i = argc - 1; i >= 1; i--)
      {
         std::cout << argv[i] << std::endl;
      }
      //std::cout << "finished!\n"; 
   }
   else
   {
      int i = 0; 
      while(envp[i] != NULL)
      {
         std::cout << envp[i] << std::endl;
         i++;
      }
   }
   std::exit( EXIT_SUCCESS );
}
